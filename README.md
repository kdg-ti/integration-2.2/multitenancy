# Multitenancy

This project features a solution for representing multiple different tenants
using a single Spring Boot application. It uses a single database schema to store the
data for all of the tenants.  
The tenants are disconnected. I.e., signing in through one tenant's URL doesn't give the user access
to the other tenant's URL, even if the user has an account on _both_ tenants using the
_same_ email address.

The implementation works domain-based. This means that the domain name is used to determine
which tenant's content is being requested.  
Locally, this solution can be tested using `localhost` variants:
* `localhost:8080`, `localhost:8080/login`, etc. all assume access to a general administration
  sub-site (so **no** tenant)
* `tenant1.localhost:8080`, `tenant1.localhost:8080/login`, etc. assumes access to tenant1's
  sub-site
* Same for all other subdomains of localhost, such as `tenant2.localhost:8080`. Of course, the
  tenant needs to exist in the database.

## Building and running

In order to build and run this application on your system, you need:
* JDK 17+
* Docker (docker daemon needs to be running)

### Database

The application uses a PostgreSQL database that can be set up as a Docker container. Run
the `Dockerfile` and  make sure to map PostgreSQL/container port 5432 to host **port 5433**.
As you can see in `application.properties`, the Spring Boot application connects to DB port 5433.
This is because you may be running a real PostgreSQL on your system on port 5432 (as I do).  

Once the DB is up and running, you can start the Spring Boot application. No Spring profile
has been set up.

#### Commands

From the project root, create the docker image. It can be created from the `Dockerfile`:
```shell
docker build -t "tenant_db_image:Dockerfile" .
```

Next, create a container. Here, I'm mapping the container's port 5432 to the host's port
5433 (since 5432 is already occupied on my machine). I'm giving the container
the name `tenant_db_container`:
```shell
docker create --name tenant_db_container -p 5433:5432 tenant_db_image:Dockerfile
```

Start the container:
```shell
docker container start tenant_db_container
```

Optional: enable auto-start for this container:
```shell
docker update --restart unless-stopped tenant_db_container
```

### Spring Boot Application

#### Commands

```shell
./gradlew bootRun
```

## Using the application

You can manually test the application by pointing your browser at `http://localhost:8080`.
