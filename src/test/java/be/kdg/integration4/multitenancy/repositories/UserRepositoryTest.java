package be.kdg.integration4.multitenancy.repositories;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import be.kdg.integration4.multitenancy.domain.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void dataSqlShouldBeUsedForInitialization() {
        var larsAtTenant1 = userRepository.findUser("lars@somehost.com", "tenant1");
        var larsAtTenant2 = userRepository.findUser("lars@somehost.com", "tenant2");
        var larsAsGlobalAdmin = userRepository.findGeneralAdmin("lars@somehost.com");
        var jefAtTenant1 = userRepository.findUser("jef@somehost.com", "tenant1");
        var jefAtTenant2 = userRepository.findUser("jef@somehost.com", "tenant2");
        var jefAsGlobalAdmin = userRepository.findGeneralAdmin("jef@somehost.com");
        var adminAtTenant1 = userRepository.findUser("admin@somehost.com", "tenant1");
        var adminAtTenant2 = userRepository.findUser("admin@somehost.com", "tenant2");
        var adminAsGlobalAdmin = userRepository.findGeneralAdmin("admin@somehost.com");

        assertTrue(larsAtTenant1.isPresent());
        assertEquals(Role.USER, larsAtTenant1.get().getRole());
        assertEquals("tenant1", larsAtTenant1.get().getTenant().getSlug());
        assertEquals("lars@somehost.com", larsAtTenant1.get().getEmail());
        assertEquals("$2a$10$lHDYiGTTEg5XJ0Ff/FcZVuxKb8ACj5bJ2krZeypHuk3KEDEW/CQTe", larsAtTenant1.get().getPassword());
        assertTrue(larsAtTenant2.isPresent());
        assertEquals(Role.TENANT_ADMIN, larsAtTenant2.get().getRole());
        assertEquals("tenant2", larsAtTenant2.get().getTenant().getSlug());
        assertEquals("lars@somehost.com", larsAtTenant2.get().getEmail());
        assertEquals("$2a$10$lHDYiGTTEg5XJ0Ff/FcZVuxKb8ACj5bJ2krZeypHuk3KEDEW/CQTe", larsAtTenant2.get().getPassword());
        assertTrue(larsAsGlobalAdmin.isEmpty());
        assertTrue(jefAtTenant1.isPresent());
        assertEquals(Role.USER, jefAtTenant1.get().getRole());
        assertEquals("tenant1", jefAtTenant1.get().getTenant().getSlug());
        assertEquals("jef@somehost.com", jefAtTenant1.get().getEmail());
        assertEquals("$2a$10$0gKyJKEVtTpPITLSNkXfwOuDFTef2MgXAQ60cyKNkRi2C2hYkjdsO", jefAtTenant1.get().getPassword());
        assertTrue(jefAtTenant2.isPresent());
        assertEquals(Role.USER, jefAtTenant2.get().getRole());
        assertEquals("tenant2", jefAtTenant2.get().getTenant().getSlug());
        assertEquals("jef@somehost.com", jefAtTenant2.get().getEmail());
        assertEquals("$2a$10$0gKyJKEVtTpPITLSNkXfwOuDFTef2MgXAQ60cyKNkRi2C2hYkjdsO", jefAtTenant2.get().getPassword());
        assertTrue(jefAsGlobalAdmin.isEmpty());
        assertTrue(adminAtTenant1.isEmpty());
        assertTrue(adminAtTenant2.isEmpty());
        assertTrue(adminAsGlobalAdmin.isPresent());
        assertEquals(Role.ADMINISTRATOR, adminAsGlobalAdmin.get().getRole());
        assertNull(adminAsGlobalAdmin.get().getTenant());
        assertEquals("admin@somehost.com", adminAsGlobalAdmin.get().getEmail());
        assertEquals("$2a$10$l.Y/.kFZ35v2Qm1DN5SXo.wYLcQx6aDP0GOWGw7q6/FEITMHtAn0S", adminAsGlobalAdmin.get().getPassword());
    }
}
