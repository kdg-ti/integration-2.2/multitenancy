INSERT INTO tenant(slug, name)
    VALUES('tenant1', 'The first tenant');
INSERT INTO tenant(slug, name)
    VALUES('tenant2', 'The second tenant');

/* tenant1 / lars@somehost.com / lars / USER */
INSERT INTO application_user(tenant_id, email, password, role)
    VALUES((SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant1'),
           'lars@somehost.com',
           '$2a$10$lHDYiGTTEg5XJ0Ff/FcZVuxKb8ACj5bJ2krZeypHuk3KEDEW/CQTe',
           2); /* 2 is USER */
/* tenant2 / lars@somehost.com / lars / TENANT_ADMIN */
INSERT INTO application_user(tenant_id, email, password, role)
    VALUES((SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant2'),
           'lars@somehost.com',
           '$2a$10$lHDYiGTTEg5XJ0Ff/FcZVuxKb8ACj5bJ2krZeypHuk3KEDEW/CQTe',
           1); /* 1 is TENANT_ADMIN */
/* admin@somehost.com / admin / ADMINISTRATOR */
INSERT INTO application_user(tenant_id, email, password, role)
    VALUES(NULL,
           'admin@somehost.com',
           '$2a$10$l.Y/.kFZ35v2Qm1DN5SXo.wYLcQx6aDP0GOWGw7q6/FEITMHtAn0S',
           0); /* 0 is ADMINISTRATOR */
/* tenant1 / jef@somehost.com / jef / USER */
INSERT INTO application_user(tenant_id, email, password, role)
    VALUES((SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant1'),
           'jef@somehost.com',
           '$2a$10$0gKyJKEVtTpPITLSNkXfwOuDFTef2MgXAQ60cyKNkRi2C2hYkjdsO',
           2); /* 2 is USER */
/* tenant2 / jef@somehost.com / jef / USER */
INSERT INTO application_user(tenant_id, email, password, role)
    VALUES((SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant2'),
           'jef@somehost.com',
           '$2a$10$0gKyJKEVtTpPITLSNkXfwOuDFTef2MgXAQ60cyKNkRi2C2hYkjdsO',
           2); /* 2 is USER */

INSERT INTO post(text, tenant_id, author_id)
    VALUES('Tenant 1 is the best tenant!',
           (SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant1'),
           (SELECT usr.id FROM application_user AS usr WHERE usr.email = 'lars@somehost.com'
                   AND usr.tenant_id = (SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant1')));
INSERT INTO post(text, tenant_id, author_id)
    VALUES('I really like what tenant 1 is doing...',
           (SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant1'),
           (SELECT usr.id FROM application_user AS usr WHERE usr.email = 'jef@somehost.com'
                   AND usr.tenant_id = (SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant1')));
INSERT INTO post(text, tenant_id, author_id)
    VALUES('Honestly, there''s no tenant like tenant 2... and I mean that in a good way!',
           (SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant2'),
           (SELECT usr.id FROM application_user AS usr WHERE usr.email = 'lars@somehost.com'
                   AND usr.tenant_id = (SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant2')));
INSERT INTO post(text, tenant_id, author_id)
    VALUES('Tenant 2 is GOAT',
           (SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant2'),
           (SELECT usr.id FROM application_user AS usr WHERE usr.email = 'jef@somehost.com'
                   AND usr.tenant_id = (SELECT ten.id FROM tenant AS ten WHERE ten.slug = 'tenant2')));
