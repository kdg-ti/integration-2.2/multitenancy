/**
 * @type {NodeListOf<HTMLButtonElement>}
 */
const allDeleteButtons = document.querySelectorAll('tr[id^="post_"] > td > button');

for (const button of allDeleteButtons) {
    button.addEventListener('click', deletePost);
}

/**
 * @param {Event} event
 */
function deletePost(event) {
    const tableRow = event.target.parentNode.parentNode;
    const postId = parseInt(tableRow.id.substring(tableRow.id.indexOf('_') + 1));
    const csrfHeader = document.querySelector('meta[name="_csrf_header"]').content;
    const csrfToken = document.querySelector('meta[name="_csrf"]').content;
    fetch(`/api/posts/${postId}`, {
        method: "DELETE",
        headers: {
            [csrfHeader]: csrfToken
        }
    }).then(handleDeleteRequest);
}

/**
 * @param {Response} response
 */
function handleDeleteRequest(response) {
    if (response.status === 204) {
        const postId = parseInt(response.url.substring(response.url.lastIndexOf('/') + 1));
        const tableRow = document.querySelector(`#post_${postId}`);
        tableRow.parentNode.removeChild(tableRow);
    }
}
