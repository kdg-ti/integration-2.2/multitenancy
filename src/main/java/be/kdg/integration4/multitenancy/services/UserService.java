package be.kdg.integration4.multitenancy.services;

import be.kdg.integration4.multitenancy.domain.Role;
import be.kdg.integration4.multitenancy.domain.User;
import be.kdg.integration4.multitenancy.exceptions.TenantNotFoundException;
import be.kdg.integration4.multitenancy.repositories.TenantRepository;
import be.kdg.integration4.multitenancy.repositories.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final TenantRepository tenantRepository;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public UserService(TenantRepository tenantRepository,
                       UserRepository userRepository,
                       BCryptPasswordEncoder passwordEncoder) {
        this.tenantRepository = tenantRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void createNewUser(String email, String password, long tenantId, Role role) {
        var tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new TenantNotFoundException(
                        "Tenant " + tenantId + " not found."));
        userRepository.save(new User(tenant, email, passwordEncoder.encode(password), role));
    }
}
