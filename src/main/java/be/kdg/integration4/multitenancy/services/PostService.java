package be.kdg.integration4.multitenancy.services;

import be.kdg.integration4.multitenancy.domain.Post;
import be.kdg.integration4.multitenancy.exceptions.TenantNotFoundException;
import be.kdg.integration4.multitenancy.exceptions.UserNotFoundException;
import be.kdg.integration4.multitenancy.repositories.PostRepository;
import be.kdg.integration4.multitenancy.repositories.TenantRepository;
import be.kdg.integration4.multitenancy.repositories.UserRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class PostService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final TenantRepository tenantRepository;

    public PostService(PostRepository postRepository, UserRepository userRepository,
                       TenantRepository tenantRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.tenantRepository = tenantRepository;
    }

    public void addPost(long userId, long tenantId, String text) {
        var user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User " + userId + " not found."));
        var tenant =
                tenantRepository.findById(tenantId).orElseThrow(() -> new TenantNotFoundException(
                        "Tenant " + tenantId + " not found."));
        postRepository.save(new Post(text, tenant, user));
    }

    public List<Post> getPostsOfTenantWithAuthor(long tenantId) {
        return postRepository.findByTenantIdWithAuthor(tenantId);
    }

    public boolean removePost(long postId, long tenantId) {
        var post = postRepository.findByIdAndTenantId(postId, tenantId);
        if (post.isEmpty()) {
            return false;
        }
        postRepository.delete(post.get());
        return true;
    }
}
