package be.kdg.integration4.multitenancy.controllers.mvc;

import be.kdg.integration4.multitenancy.controllers.mvc.viewModels.NewUserViewModel;
import be.kdg.integration4.multitenancy.domain.Role;
import be.kdg.integration4.multitenancy.services.UserService;
import be.kdg.integration4.multitenancy.tenants.TenantId;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {
    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("login")
    public String showLoginPage() {
        return "login";
    }

    @GetMapping("register")
    public String showRegisterPage() {
        return "register";
    }

    @PostMapping("register")
    public String registerNewUser(@Valid NewUserViewModel userViewModel,
                                  @TenantId long tenantId,
                                  HttpServletRequest request)
            throws ServletException {
        userService.createNewUser(
                userViewModel.getUsername(),
                userViewModel.getPassword1(),
                tenantId,
                Role.USER);
        request.login(userViewModel.getUsername(), userViewModel.getPassword1());

        return "redirect:/";
    }
}
