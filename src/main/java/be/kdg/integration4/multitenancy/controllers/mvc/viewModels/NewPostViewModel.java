package be.kdg.integration4.multitenancy.controllers.mvc.viewModels;

import jakarta.validation.constraints.NotBlank;

public class NewPostViewModel {
    @NotBlank
    private String text;

    public NewPostViewModel() {
    }

    public NewPostViewModel(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
