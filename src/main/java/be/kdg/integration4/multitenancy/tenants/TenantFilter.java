package be.kdg.integration4.multitenancy.tenants;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import be.kdg.integration4.multitenancy.domain.Tenant;
import be.kdg.integration4.multitenancy.repositories.TenantRepository;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

public class TenantFilter extends OncePerRequestFilter {
    private static final Logger LOGGER = Logger.getLogger(TenantFilter.class.getName());

    private final TenantRepository tenantRepository;

    public TenantFilter(TenantRepository tenantRepository) {
        this.tenantRepository = tenantRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {
        var tenant = getTenant(request);
        var tenantId = tenantRepository.findBySlug(tenant).map(Tenant::getId).orElse(null);
        if (tenant != null && tenantId == null) {
            // Attempted access to non-existing tenant
            response.setStatus(NOT_FOUND.value());
            return;
        }
        LOGGER.info("Setting tenant: " + tenant + " (domain " + request.getServerName() + ")");
        LOGGER.info("Setting tenant ID: " + tenantId);
        TenantContext.setCurrentTenant(tenant);
        TenantContext.setCurrentTenantId(tenantId);
        chain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return request.getRequestURI().startsWith("/webjars/")
                || request.getRequestURI().startsWith("/css/")
                || request.getRequestURI().startsWith("/js/")
                || request.getRequestURI().endsWith(".ico");
    }

    private String getTenant(HttpServletRequest request) {
        var domain = request.getServerName();
        var dotIndex = domain.indexOf(".");

        String tenant = null;
        if (dotIndex != -1) {
            tenant = domain.substring(0, dotIndex);
        }

        return tenant;
    }
}
